<?php 

namespace Ppast\Core\Frontend\Tests;


use \Ppast\Core\Frontend\ParamsManager;
use \PHPUnit\Framework\TestCase;



class PMT extends ParamsManager
{
	/**
	 * Enumérer les paramètres
	 *
	 * @param string $group Groupe de paramètres à énumérer
	 * @param string[] $onlyKeys Liste restrictives des paramètres à renvoyer (sinon, toute la liste est renvoyée)
	 * @return array Renvoie un tableau associatif (clef => valeur)
	 */
	public function enum($group, $onlyKeys = []) {}

	
	
	/**
	 * Obtenir un paramètre dans un groupe
	 *
	 * @param string $group Groupe concerné
	 * @param string $key Clef dans le groupe $group
	 * @param mixed $defv Valeur par défaut
	 * @return mixed Valeur demandée
	 */
	public function get ($group, $key, $defv = NULL) {}

	
	
	/**
	 * Définir un paramètre dans un groupe
	 *
	 * @param string $group Groupe concerné
	 * @param string $key Clef dans le groupe $group
	 * @param mixed $val Valeur à définir
	 */	
	public function set ($group, $key, $val) {}
}






class ParamsManagerTest extends TestCase
{
	function testParamsManager()
	{
		// créer mock
		$m = $this->getMockBuilder(PMT::class)->onlyMethods(['enum', 'get', 'set'])->getMock();
		$m->method('get')->with($this->equalTo('gr1'), $this->equalTo('key1'), $this->equalTo('default'))->willReturn('val1');

		// vérifier obtention valeurs
		$this->assertEquals('val1', $m->get('gr1', 'key1', 'default'));
		
		// vérifier SET appelé exactement 2 fois
		$m->expects($this->exactly(2))->method('set')->with($this->equalTo('gr1'), $this->equalTo('key1'), $this->equalTo('value'));
		$m->set('gr1', 'key1', 'value');
		$m->gr1_key1 = 'value';
		
		
		// obtention par accesseur magique
		$m = $this->getMockBuilder(PMT::class)->onlyMethods(['enum', 'get', 'set'])->getMock();
		$m->method('get')->with($this->equalTo('gr1'), $this->equalTo('key1'))->willReturn('val1');
		$this->assertEquals('val1', $m->gr1_key1);
				
		
		// obtention par accesseur magique, groupe = null
		$m = $this->getMockBuilder(PMT::class)->onlyMethods(['enum', 'get', 'set'])->getMock();
		$m->method('get')->with($this->equalTo(null), $this->equalTo('key1'))->willReturn('val0');
		$this->assertEquals('val0', $m->key1);
				
		
		// obtention par accesseur magique ; vérifier qu'un _ dans le nom de la clef ne pose pas de problème à la séparation groupe/clef
		$m = $this->getMockBuilder(PMT::class)->onlyMethods(['enum', 'get', 'set'])->getMock();
		$m->method('get')->with($this->equalTo('gr1'), $this->equalTo('sub_key1'))->willReturn('val1');
		$this->assertEquals('val1', $m->gr1_sub_key1);
		
		
		// tester énum
		$m = $this->getMockBuilder(PMT::class)->onlyMethods(['enum', 'get', 'set'])->getMock();
		$m->method('enum')->with($this->equalTo('gr1'))->willReturn(['k1'=>'val1', 'k2'=>'val2']);
		$r = $m->setupRegistry('gr1');
		$v1 = base64_encode('val1');
		$v2 = base64_encode('val2');
		$this->assertEquals(true, strpos($r, "new ppast.ParamsManager.Registry('gr1', {\"k1\":\"$v1\",\"k2\":\"$v2\"});") !== FALSE);
	}
	


}

?>
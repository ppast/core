<?php 

namespace Ppast\Core\MailSendersRegistry\Tests;


use \PHPUnit\Framework\TestCase;
use \Ppast\Core\MailSendersRegistry\JsonRegistry;
use \Ppast\Core\MailSendersRegistry\Parameters;
use \Ppast\Core\MailSendersRegistry\Facade;



class FacadeTest extends TestCase
{
	function testRegistry()
	{
		$json = '{"SMTP:mailing":{"host":"smtp.domain.eu", "className":"SMTP"}, "PHPMail":{}}';
		$r = new JsonRegistry('SMTP:mailing', $json);
		
		$f = new Facade($r);
		
		$this->assertInstanceOf(\Nettools\Mailing\MailSenders\MailSender::class, $f->getActive());
		
		$this->assertEquals(true, is_array($f->enum()));
		$this->assertEquals(2, count($f->enum()));
		$this->assertInstanceOf(Parameters::class, $f->enum()[0]);
		$this->assertInstanceOf(Parameters::class, $f->enum()[1]);
		$this->assertEquals('SMTP:mailing', $f->enum()[0]->name);
		$this->assertEquals('SMTP', $f->enum()[0]->className);
		$this->assertEquals('PHPMail', $f->enum()[1]->name);
		$this->assertEquals('PHPMail', $f->enum()[1]->className);
	}

}

?>
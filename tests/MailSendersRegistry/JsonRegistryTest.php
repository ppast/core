<?php 

namespace Ppast\Core\MailSendersRegistry\Tests;


use \PHPUnit\Framework\TestCase;
use \Ppast\Core\MailSendersRegistry\JsonRegistry;
use \Ppast\Core\MailSendersRegistry\Parameters;



class JsonRegistryTest extends TestCase
{
	function testRegistry()
	{
		$json = '{"SMTP:mailing":{"host":"smtp.domain.eu", "className":"SMTP"}, "PHPMail":{}}';
		$r = new JsonRegistry('SMTP:mailing', $json);
		
		$this->assertInstanceOf(JsonRegistry::class, $r);
		$this->assertInstanceOf(Parameters::class, $r->getActiveParameters());
		$this->assertEquals('SMTP:mailing', $r->getActiveParameters()->name);
		$this->assertEquals('SMTP', $r->getActiveParameters()->className);
		$this->assertEquals(['host'=>'smtp.domain.eu', 'className'=>'SMTP'], $r->getActiveParameters()->data);
		
		
		$this->assertEquals(true, is_array($r->enum()));
		$this->assertEquals(2, count($r->enum()));
		$this->assertInstanceOf(Parameters::class, $r->enum()[0]);
		$this->assertInstanceOf(Parameters::class, $r->enum()[1]);
		$this->assertEquals('SMTP:mailing', $r->enum()[0]->name);
		$this->assertEquals('SMTP', $r->enum()[0]->className);
		$this->assertEquals('PHPMail', $r->enum()[1]->name);
		$this->assertEquals('PHPMail', $r->enum()[1]->className);
	}

}

?>
<?php 

namespace Ppast\Core\MailSendersRegistry\Tests;


use \PHPUnit\Framework\TestCase;
use \Nettools\Mailing\MailBuilder\TextPlainContent;
use \Nettools\Mailing\Mailer;
use \Ppast\Core\MailSendersRegistry\JsonRegistry;
use \Ppast\Core\MailSendersRegistry\Parameters;
use \Ppast\Core\MailSendersRegistry\Facade;
use \Ppast\Core\MailSendersRegistry\QuotaFacade;
use \Ppast\Core\MailSendersRegistry\QuotaInterface;



class QI implements QuotaInterface
{
	public $data = [];
	
	
	/**
	 * Ajouter une valeur au quota 
	 *
	 * @param string $name Nom de la stratégie d'envois pour laquelle augmenter le quota
	 */
	function add($name)
	{
		if ( !array_key_exists($name, $this->data) )
			$this->data[$name] = [];
				
		$this->data[$name][] = time();
	}
	
	
	
	/**
	 * Nettoyer le stockage des quotas antérieurs à la date donnée
	 *
	 * @param int $dt
	 */
	function clean($dt)
	{
		foreach ( $this->data as $k => $quotas )
			$this->data[$k] = array_values(array_filter($quotas, function($ts) use ($dt) { return $ts > $dt; } ));
	}
	
	
	
	/**
	 * Obtenir la liste des quotas stockés sous forme de tableau associatif [ nom_stratégie_1 => [ timestamp1, timestamp2, ... ], nom_stratégie_2 => [ ts1, ts2, ... ], ... ]
	 *
	 * @return array
	 */
	function get()
	{
		return $this->data;
	}
}





class QuotaFacadeTest extends TestCase
{
	function testQuotaAdd()
	{
		$qi = new QI();
		
		
		$json = '{"Virtual":{"quota":"100:d"}, "PHPMail":{}}';
		$r = new JsonRegistry('Virtual', $json);		
		$f = new QuotaFacade(new Facade($r), $qi);
		
		$this->assertInstanceOf(\Nettools\Mailing\MailSenders\MailSender::class, $f->getActive());
		
		$this->assertEquals(true, is_array($f->enum()));
		$this->assertEquals('100:d', $f->enum()[0]->data['quota']);
		
		
		$ms = $f->getActive();
		$ml = new Mailer($ms);

		$m = new TextPlainContent('textplain content');
		$ml->sendmail($m, 'unit-test@php.com', 'unit-test-recipient@php.com', 'Mail subject', false);
		$sent = $ms->getSent();
		
		$this->assertEquals(1, count($sent));
		$this->assertEquals(['Virtual'], array_keys($qi->data));
		$this->assertEquals(true, is_array($qi->data['Virtual']));
		$this->assertEquals(1, count($qi->data['Virtual']));
		$this->assertEquals(true, $qi->data['Virtual'][0] >= time());

		
		$m = new TextPlainContent('textplain content2');
		$ml->sendmail($m, 'unit-test@php.com', 'unit-test-recipient@php.com', 'Mail subject 2', false);
		$sent = $ms->getSent();

		$this->assertEquals(2, count($sent));
		$this->assertEquals(['Virtual'], array_keys($qi->data));
		$this->assertEquals(true, is_array($qi->data['Virtual']));
		$this->assertEquals(2, count($qi->data['Virtual']));
		
	}

	
	
	function testQuotaCompute()
	{
		$qi = new QI();
		$qi->add('Virtual');
		$qi->add('Virtual');
		
		
		$json = '{"Virtual":{"quota":"10:d"}, "PHPMail":{}}';
		$r = new JsonRegistry('Virtual', $json);		
		$f = new QuotaFacade(new Facade($r), $qi);
		
		$quotas = $f->compute();
		
		$this->assertEquals(true, array_key_exists('Virtual', $quotas));
		$this->assertEquals(20, $quotas['Virtual']->pct);
		$this->assertEquals(2, $quotas['Virtual']->value);
		$this->assertEquals('10', $quotas['Virtual']->quota);
		$this->assertEquals('d', $quotas['Virtual']->period);

		$this->assertEquals(false, array_key_exists('PHPMail', $quotas));
	}
	

	
	function testQuotaClean()
	{
		$qi = new QI();
		$t = time();
		$ts = $t-48*60*60;
		$qi->data['Virtual'] = [$ts, $ts+3600, $t];
		$qi->clean(strtotime('today'));
		
		$this->assertEquals(['Virtual' => [$t]], $qi->data);
	}
	

	
	function testQuotaClean2()
	{
		$qi = new QI();
		$t = time();
		$ts = $t-48*60*60;
		$qi->data['Virtual'] = [$ts, $ts+3600, $t];
		
		
		// obtenir stratégie active, cela nettoie automatiquement le quota
		$json = '{"Virtual":{"quota":"10:d"}, "PHPMail":{}}';
		$r = new JsonRegistry('Virtual', $json);
		$f = new QuotaFacade(new Facade($r), $qi);

		$ms = $f->getActive();
		$this->assertEquals(['Virtual' => [$t]], $qi->data);
	}

}

?>
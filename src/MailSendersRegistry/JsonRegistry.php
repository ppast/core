<?php

// namespace
namespace Ppast\Core\MailSendersRegistry;






// classe d'aide pour registre json d'interfaces Nettools\Mailing\MailSenders\MailSender
class JsonRegistry extends ArrayRegistry{
	
	/**
	 * Convertir un objet json décrivant les jeux de paramètres pour les stratégies d'envois en liste d'objets Parameters
	 *
	 * @param string $json
	 * @return Parameters[]
	 */
	static function decodeJson($json)
	{
		$js = json_decode($json);
		if ( is_null($js) )
			throw new Exception('JsonRegistry : erreur de décodage structure json');
		
		
		// décoder la liste json, chaque propriété est le nom du jeu de paramètres, la valeur est un objet donnant les paramètres
		$ret = [];
		foreach ( $js as $prop => $val )
			$ret[] = new Parameters($prop, get_object_vars($val));
		
		return $ret;
	}
	
	
	
	/**
	 * Constructeur
	 * 
	 * @param string $active Nom du jeu de paramètres actif
	 * @param string $json Structure json contenant la description des stratégies d'envoi
	 */
	function __construct($active, $json)
	{
		parent::__construct($active, self::decodeJson($json));
	}
}

?>
<?php

// namespace
namespace Ppast\Core\MailSendersRegistry;






// classe de définition des paramètres d'instantiation pour une stratégie d'envois Nettools\Mailing\MailSenders\MailSender
class Parameters{
	
	public $name;
	public $className;
	public $data = null;
	
	
	
	
	/**
	 * Constructeur
	 *
	 * Si $data contient une valeur `className` alors celle-ci sera définie pour la propriété $className ; sinon, $className = $name
	 *
	 * @param string $name Nom du jeu de paramètres (ex. SMTP:aws) 
	 * @param string[] $data
	 */
	function __construct($name, array $data)
	{
		$this->name = $name;
		$this->className = array_key_exists('className', $data) ? $data['className'] : $name;
		$this->data = $data;
	}
}

?>
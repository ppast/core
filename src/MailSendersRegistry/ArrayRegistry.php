<?php

// namespace
namespace Ppast\Core\MailSendersRegistry;






// classe d'aide pour registre tableau d'interfaces Nettools\Mailing\MailSenders\MailSender
class ArrayRegistry implements Registry{
	
	protected $lst = [];
	protected $active = NULL;
	
	
	
	/**
	 * Constructeur
	 * 
	 * @param string $active Nom du jeu de paramètres actif dans $lst
	 * @param Parameters[] $lst Liste d'objets Parameters décrivant les stratégies d'envoi
	 */
	function __construct($active, array $lst)
	{
		$this->lst = $lst;
		
		foreach ( $this->lst as $p )
			if ( $p->name == $active )
				$this->active = $p;
		
		
		if ( !$this->active )
			throw new Exception('Aucun jeu de paramètres par défaut défini');
	}
	
	
	
	/**
	 * Enumérer les interfaces disponibles
	 *
	 * @return Parameters[]
	 */
	function enum ()
	{
		return $this->lst;
	}
	
	
	
	/** 
	 * Obtenir le jeu de paramètres pour l'interface demandée
	 *
	 * @param string $name Nom du jeu de paramètres (ex. SMTP:aws) à obtenir
	 * @return Parameters
	 */
	function getParameters($name)
	{
		foreach ( $this->lst as $p )
			if ( $p->name == $name )
				return $p;
		
		return NULL;
	}
	
	
	
	/** 
	 * Obtenir le jeu de paramètres pour l'interface définie comme active
	 *
	 * @return Parameters
	 */
	function getActiveParameters()
	{
		return $this->active;
	}
}

?>
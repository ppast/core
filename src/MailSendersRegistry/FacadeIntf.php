<?php

// namespace
namespace Ppast\Core\MailSendersRegistry;






// Interface d'aide pour définition facade
interface FacadeIntf{

	/**
	 * Enumérer les interfaces disponibles
	 *
	 * @return Parameters[]
	 */
	function enum ();
	
	
	
	/** 
	 * Obtenir un objet Nettools\Mailing\MailSenders\MailSender pour l'interface définie comme active
	 *
	 * @return \Nettools\Mailing\MailSenders\MailSender
	 */
	function getActive();
}

?>
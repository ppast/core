<?php

// namespace
namespace Ppast\Core\MailSendersRegistry;






// classe d'aide pour registre d'interfaces Nettools\Mailing\MailSenders\MailSender
class Facade implements FacadeIntf{

	protected $registry = NULL;
	
	
	
	/**
	 * Constructeur de l'interface facade
	 *
	 * @param Registry $reg 
	 */
	public function __construct(Registry $reg)
	{
		$this->registry = $reg;
	}
	
	
	
	/**
	 * Obtenir le registre
	 *
	 * @return Registry
	 */
	public function getRegistry()
	{
		return $this->registry;
	}
	
	
	
	/**
	 * Enumérer les interfaces disponibles
	 *
	 * @return Parameters[]
	 */
	function enum ()
	{
		return $this->registry->enum();
	}
	
	
	
	/** 
	 * Obtenir un objet Nettools\Mailing\MailSenders\MailSender pour l'interface définie comme active
	 *
	 * @return \Nettools\Mailing\MailSenders\MailSender
	 */
	function getActive()
	{
		// obtenir jeu de paramètres actif
		$p = $this->registry->getActiveParameters();
				
		// forger nom de classe
		$class = "\\Nettools\\Mailing\\MailSenders\\" . $p->className;

		
		try
		{
			// instancier par réflection
			return (new \ReflectionClass($class))->newInstanceArgs([$p->data]);
		}
		catch( \ReflectionException $e )
		{
			throw new Exception("MailSender of class '$class' does not exist (" . $e->getMessage() . ")");
		}
	}
}

?>
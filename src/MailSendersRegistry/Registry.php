<?php

// namespace
namespace Ppast\Core\MailSendersRegistry;






// classe d'aide pour registre d'interfaces Nettools\Mailing\MailSenders\MailSender
interface Registry{
	
	/**
	 * Enumérer les interfaces disponibles
	 *
	 * @return Parameters[]
	 */
	function enum();
	
	
	
	/** 
	 * Obtenir le jeu de paramètres pour l'interface demandée
	 *
	 * @param string $name Nom du jeu de paramètres (ex. SMTP:aws) à obtenir
	 * @return Parameters
	 */
	function getParameters($name);
	
	
	
	/** 
	 * Obtenir le jeu de paramètres pour l'interface définie comme active
	 *
	 * @return Parameters
	 */
	function getActiveParameters();
}

?>
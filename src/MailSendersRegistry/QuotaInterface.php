<?php

// namespace
namespace Ppast\Core\MailSendersRegistry;






// interface d'aide pour calcul des quotas
interface QuotaInterface{
	
	/**
	 * Ajouter une valeur au quota 
	 *
	 * @param string $name Nom de la stratégie d'envois pour laquelle augmenter le quota
	 */
	function add($name);
	
	
	/**
	 * Nettoyer le stockage des quotas antérieurs à la date donnée
	 *
	 * @param int $dt
	 */
	function clean($dt);
	
	
	/**
	 * Obtenir la liste des quotas stockés sous forme de tableau associatif [ nom_stratégie_1 => [ timestamp1, timestamp2, ... ], nom_stratégie_2 => [ ts1, ts2, ... ], ... ]
	 *
	 * @return array
	 */
	function get();
}

?>
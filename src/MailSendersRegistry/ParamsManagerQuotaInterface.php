<?php

// namespace
namespace Ppast\Core\MailSendersRegistry;


use \Ppast\Core\Frontend\ParamsManager;





// Classe d'aide pour calcul des quotas, stockés dans ParamsManager
class ParamsManagerQuotaInterface implements QuotaInterface{
	
	protected $_pm = null;
	protected $_key = null;
	
	
	
	/**
	 * Constructeur
	 *
	 * @param Ppast\Core\Frontend\ParamsManager $pm
	 * @param string $key Nom de la clef de paramétrage qui contient le stockage des quotas
	 */
	public function __construct(ParamsManager $pm, $key)
	{
		$this->_pm = $pm;
		$this->_key = $key;
	}
	
	
	
	/**
	 * Ajouter une valeur au quota 
	 *
	 * @param string $name Nom de la stratégie d'envois pour laquelle augmenter le quota
	 */
	function add($name)
	{
		$q = $this->get();
		if ( is_null($q) )
			$q = [];
			
		if ( !array_key_exists($name, $q) )
			$q[$name] = [];

		$q[$name][] = time();
		
		
		$this->_pm->set(null, $this->_key, json_encode($q));
	}
	
	
	
	/**
	 * Nettoyer le stockage des quotas antérieurs à la date donnée
	 *
	 * @param int $dt
	 */
	function clean($dt)
	{
		$q = $this->get();
		if ( is_array($q) )
		{
			foreach ( $q as $qname => $qlist )
				$q[$qname] = array_values(array_filter($qlist, function ($v) use ($dt) { return $v > $dt; }));
		}
		
		
		$this->_pm->set(null, $this->_key, json_encode($q));
	}
	
	
	
	/**
	 * Obtenir la liste des quotas stockés sous forme de tableau associatif [ nom_stratégie_1 => [ timestamp1, timestamp2, ... ], nom_stratégie_2 => [ ts1, ts2, ... ], ... ]
	 *
	 * @return array
	 */
	function get()
	{
		return json_decode($this->_pm->get(null, $this->_key, '{}'), true);
	}
}

?>
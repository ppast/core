<?php

// namespace
namespace Ppast\Core\MailSendersRegistry;


use \Nettools\Mailing\MailSenders\SentHandler;
use \Nettools\Mailing\MailerEngine\Headers;





// classe d'aide pour registre d'interfaces Nettools\Mailing\MailSenders\MailSender avec gestion du quota d'envois
class QuotaFacade implements FacadeIntf, SentHandler {

	protected $_quotaIntf;
	protected $_facade;
	
	const QUOTA = 'quota';
	
	
	
	
	/**
	 * Constructeur de l'interface facade avec quota
	 *
	 * @param Facade $f Facade sous-jacente
	 * @param QuotaInterface $qi Interface de stockage des quotas
	 */
	public function __construct(Facade $f, QuotaInterface $qi)
	{
		$this->_facade = $f;
		$this->_quotaIntf = $qi;
	}
	
		
	
	/**
	 * Enumérer les interfaces disponibles
	 *
	 * @return Parameters[]
	 */
	function enum ()
	{
		return $this->_facade->enum();
	}
	
	
	
	/** 
	 * Notify about `sent` event
	 *
     * @param string $to Recipient
     * @param string $subject Subject
     * @param Nettools\Mailing\MailerEngine\Headers $headers Email headers
	 */
	function notify($to, $subject, Headers $headers)
	{
		$this->_quotaIntf->add($this->_facade->getRegistry()->getActiveParameters()->name);
	}
	
	
	
	/**
	 * Calculer les quotas
	 *
	 * @return array Renvoie un tableau associatif [ nom_stratégie => { pct, value, quota, period }, ... ]
	 */
	function compute()
	{
		$ret = [];
		
		
		// obtenir les données brutes de quota pour chaque stratégie d'envois
		$quotas = $this->_quotaIntf->get();
		
		foreach ( $quotas as $qname => $timestamps )
		{
			// chercher la définition de la stratégie en cours
			$def = $this->_facade->getRegistry()->getParameters($qname);
			
			// si définition de quota
			if ( $def && array_key_exists(self::QUOTA, $def->data) )
			{
				// exploiter la définition
				$qdef = $def->data[self::QUOTA];
				list($quota, $period) = explode(':', $qdef);
				
				
				// calculer intervalle de temps à considérer (jour entier ou heure)
				switch ( $period )
				{
					// quota / jour
					case 'd':
						$dt1 = strtotime('today'); // today = midnight today
						$dt2 = strtotime('midnight +1 day');
						break;

					// quota / heure
					case 'h':
						$dt1 = time() - date('i') * 60 - date('s');
						$dt2 = $dt1 + 60*60;
						break;

					default:
						// non défini
						continue 2;
				}


				// calculer quota : on supprime les timestamp inutiles et on compte le nb de valeurs restantes
				$ts2 = array_values(array_filter($timestamps, function($ts) use ($dt1, $dt2) { return ($ts >= $dt1) && ($ts <= $dt2 ); }));
				$n = count($ts2);
				$ret[$qname] = (object)[ 'pct' => floor(100*$n/$quota), 'value' => $n, 'quota' => $quota, 'period' => $period];
			}
		}
		
		
		return $ret;
	}

	
	
	/** 
	 * Obtenir un objet Nettools\Mailing\MailSenders\MailSender pour l'interface définie comme active
	 *
	 * @return \Nettools\Mailing\MailSenders\MailSender
	 */
	function getActive()
	{
		// obtenir gestionnaire d'envois 
		$ms = $this->_facade->getActive();
		
		// $this implémente l'interface SentHandler et peut donc être la cible de l'évènement
		$ms->addSentEventHandler($this);
		
		// purger quotas maintenant
		$this->_quotaIntf->clean(strtotime('today'));	// minuit ce matin
		
		return $ms;
	}
}

?>
<?php

// namespace
namespace Ppast\Core\Frontend;


// clauses use
use \Nettools\Core\Helpers\PdoHelper;






// classe lecture écriture paramètres site
class DbParamsManager extends ParamsManager
{
	public $pdo = NULL;
	protected $_kvalue = NULL;
	protected $_tablename = NULL;
	protected $_kgroupe = NULL;
	protected $_kid = NULL;
	
	
	/**
	 * Constructeur
	 *
	 * @param PdoHelper $pdo Objet PDO associé
	 * @param string $kvalue Nom de la colonne 'valeur' dans la table des paramètres (par défaut : `valeur`)
	 * @param string $tablename Nom de la table des paramètres (par défaut : `Params`)
	 * @param string $kgroupe Nom de la colonne 'groupe' dans la table des paramètres (par défaut : `groupe`)
	 * @param string $kid Nom de la colonne 'clef' dans la table des paramètres (par défaut : `id`)
	 */	
	public function __construct(PdoHelper $pdo, $kvalue = 'valeur', $tablename = 'Params', $kgroupe = 'groupe', $kid = 'id')
	{
		$this->pdo = $pdo;
		$this->_kvalue = $kvalue;
		$this->_tablename = $tablename;
		$this->_kgroupe = $kgroupe;
		$this->_kid = $kid;
	}
	
	
	
	/**
	 * Obtenir un paramètre dans un groupe
	 *
	 * @param string $group Groupe concerné
	 * @param string $key Clef dans le groupe $group
	 * @param mixed $defv Valeur par défaut
	 * @return mixed Valeur demandée
	 */
	public function get($group, $key, $defv = NULL)
	{
		try
		{
			$result = $this->pdo->pdo_query_select("SELECT {$this->_kvalue} FROM {$this->_tablename} WHERE {$this->_kid} = :key LIMIT 1", array(':key'=>$key));
			
			$ret = $result->fetch(\PDO::FETCH_COLUMN, 0);
			$result->closeCursor();
			
			if ( $ret === FALSE )
				return $defv;
			else
				return $ret;
		}
		catch (\PDOException $e)
		{
			return FALSE;
		}
	}
	
	
	
	/**
	 * Définir un paramètre dans un groupe
	 *
	 * @param string $group Groupe concerné
	 * @param string $key Clef dans le groupe $group
	 * @param mixed $val Valeur à définir
	 */	
	public function set($group, $key, $val)
	{
		try
		{
			// si paramètre inexistant, le créer
			if ( $this->get($group, $key, FALSE) === FALSE )
				$this->pdo->pdo_query("INSERT INTO {$this->_tablename} SET {$this->_kvalue} = :v, {$this->_kgroupe} = :g, {$this->_kid} = :k", array(':v'=>$val, ':g'=>$group, ':k'=>$key));
			else
				$this->pdo->pdo_query("UPDATE {$this->_tablename} SET {$this->_kvalue} = :v WHERE {$this->_kid} = :k LIMIT 1", array(':v'=>$val, ':k'=>$key));
				
			return true;
		}
		catch (\PDOException $e)
		{
			return false;
		}
	}
	

	
	/**
	 * Enumérer les paramètres
	 *
	 * @param string $group Groupe de paramètres à énumérer
	 * @param string[] $onlyKeys Liste restrictives des paramètres à renvoyer (sinon, toute la liste est renvoyée)
	 * @return array Renvoie un tableau associatif (clef => valeur)
	 */
	public function enum($group, $onlyKeys = [])
	{
		$ret = array();
		
		try
		{
			// si restriction sur les clefs à renvoyer
			if ( count($onlyKeys) )
			{
				$placeholders = str_repeat('?,', count($onlyKeys)-1) . '?';
				$where = "{$this->_kgroupe} = ? AND {$this->_kid} IN ($placeholders)";
				$valeurs = array_merge([$group], $onlyKeys);
			}
			else
			{
				$where = "{$this->_kgroupe} = ?";
				$valeurs = [$group];
			}
			
			
			// déterminer quelles valeurs renvoyer ; si private = true, inclure valeurs privées en + des publiques ; si private = false, uniquement valeurs publiques
			$result = $this->pdo->pdo_query_select("SELECT {$this->_kid}, {$this->_kvalue} FROM {$this->_tablename} WHERE $where", $valeurs);
			while ( $row = $result->fetch(\PDO::FETCH_ASSOC) )
				$ret[$row[$this->_kid]] = $row[$this->_kvalue];
			$result->closeCursor();

			return $ret;
		}
		catch (\PDOException $e)
		{
			return NULL;
		}
	}
}


?>
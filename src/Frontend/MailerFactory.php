<?php

// namespace
namespace Ppast\Core\Frontend;



// clauses use
use Nettools\Mailing\Mailer;





// classe d'aide pour création d'un objet Mailer par défaut
class MailerFactory 
{
	/**
	 * Obtenir une instance Mailer par défaut
	 *
	 * @return Mailer
	 */
	function get()
	{
		return Mailer::getDefault();
	}
	
}


?>
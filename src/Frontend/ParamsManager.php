<?php

// namespace
namespace Ppast\Core\Frontend;







// classe lecture/écriture paramètres site
abstract class ParamsManager
{
	/**
	 * Découper une chaîne de requête en groupe et clef dans le groupe
	 * 
	 * @param string $key Chaine indiquant le groupe et la clef dans le groupe sous la forme "groupe_clef"
	 * @return object|bool Renvoie un littéral objet avec les propriétés 'group' et 'key' définies, ou false si $key incorrect
	 */
	protected function _explodeKey($key)
	{
		$t = explode('_', $key, 2);
		if ( count($t) == 2 )
			return (object)array(
							"group" => $t[0],
							"key" => $t[1]
						);
		else if ( (count($t) == 1) && $t[0] )
			return (object)[ 'group' => null, 'key' => $t[0] ];
		else
			return false;
	}
	
	
	
	/**
	 * Accesseur magique
	 *
	 * @param string $key Chaine indiquant le groupe et la clef dans le groupe sous la forme "groupe_clef"
	 * @return mixed Valeur demandée ou NULL si inexistant
	 */
	public function __get($key)
	{
		if ( $k = self::_explodeKey($key) )
			return $this->get($k->group, $k->key);
		else
			return NULL;
	}


	
	/**
	 * Accesseur magique
	 *
	 * @param string $key Chaine indiquant le groupe et la clef dans le groupe sous la forme "groupe_clef"
	 * @param mixed $val Valeur à définir
	 */
	public function __set($key, $val)
	{
		if ( $k = self::_explodeKey($key) )
			return $this->set($k->group, $k->key, $val);
		else
			return NULL;
	}

	
	
	/**
	 * Obtenir code JS de définition du registre
	 * 
	 * @param string $group Groupe de paramètres
	 * @param string[] $onlyKeys Liste restrictives des paramètres à renvoyer (sinon, toute la liste est renvoyée)
	 * @return string
	 */
	public function setupRegistry($group, $onlyKeys = [])
	{
		// obtenir tableau de valeurs
		$values = $this->enum($group);
		if ( !is_array($values) )
			$values = [];
		
		foreach ( $values as $k => $v )
			$values[$k] = base64_encode($v);
		

$js = <<<JS
// [=REGISTRY=]

window.ppast = window.ppast || {};


ppast.ParamsManager.Registry = ppast.ParamsManager.Registry || function(domain, items) {
	
	var _domain = domain;
	

	this.get = function(name, def) 
		{
			if ( (this[name] === null) || (this[name] === undefined) )
				return def;
			else
				return this[name];
		};
			
    
	this.getDomain = function() { return _domain; };
		
	for ( var it in items )
		this[it] = atob(items[it]);
	
		
	ppast.ParamsManager.Registry[domain] = this;
		
};

// [=/REGISTRY=]

JS;

		return $js . "new ppast.ParamsManager.Registry('$group', " . json_encode($values) . ");\n";
	}
	
	
	
	/**
	 * Enumérer les paramètres
	 *
	 * @param string $group Groupe de paramètres à énumérer
	 * @param string[] $onlyKeys Liste restrictives des paramètres à renvoyer (sinon, toute la liste est renvoyée)
	 * @return array Renvoie un tableau associatif (clef => valeur)
	 */
	abstract public function enum($group, $onlyKeys = []);

	
	
	/**
	 * Obtenir un paramètre dans un groupe
	 *
	 * @param string $group Groupe concerné
	 * @param string $key Clef dans le groupe $group
	 * @param mixed $defv Valeur par défaut
	 * @return mixed Valeur demandée
	 */
	abstract public function get ($group, $key, $defv = NULL);

	
	
	/**
	 * Définir un paramètre dans un groupe
	 *
	 * @param string $group Groupe concerné
	 * @param string $key Clef dans le groupe $group
	 * @param mixed $val Valeur à définir
	 */	
	abstract public function set ($group, $key, $val);
}


?>
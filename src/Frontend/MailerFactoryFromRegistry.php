<?php

// namespace
namespace Ppast\Core\Frontend;



// clauses use
use Nettools\Mailing\Mailer;
use Ppast\Core\MailSendersRegistry\Facade;
use Ppast\Core\MailSendersRegistry\Registry;
use Ppast\Core\MailSendersRegistry\JsonRegistry;
use Ppast\Core\MailSendersRegistry\ParamsManagerQuotaInterface;





// classe d'aide pour création d'un objet Mailer personnalisé depuis configuration registre applicatif
class MailerFactoryFromRegistry extends MailerFactory
{
	protected $pm;
	protected $key;
	protected $data;
	protected $cache;
	
	
	
	/**
	 * Obtenir un objet facade approprié
	 *
	 * @param Registry $reg
	 * @return Ppast\Core\MailSendersRegistry\Facade
	 */
	protected function _getFacade(Registry $reg)
	{
		return new Facade($reg);
	}
	
	
	
	/**
	 * Constructeur
	 *
	 * @param ParamsManager $pm Objet d'accès aux paramètres
	 * @param string $key Clef de registre pour stratégie d'envoi active
	 * @param string $data Clef de regitre pour données de configuration pour les stratégies configurées
	 */
	public function __construct(ParamsManager $pm, $key = 'msender', $data = 'msenders_data')
	{
		$this->pm = $pm;
		$this->key = $key;
		$this->data = $data;
		$this->cache = null;
	}
	
	
	
	/**
	 * Obtenir objet facade pour gestionnaire d'envois
	 *
	 * @return null|Ppast\Core\MailSendersRegistry\FacadeIntf
	 */
	public function getFacade()
	{
		// lire infos msender, msenders_data depuis paramétrage
		$ms = $this->pm->get(null, $this->key);
		$msdata = $this->pm->get(null, $this->data);
			
		if ( $ms && $msdata )
			// créer facade d'accès au registre des stratégies d'envois MailSender
			return $this->_getFacade(new JsonRegistry($ms, $msdata));
		else
			return null;
	}
	
	
	
	/**
	 * Obtenir une instance Mailer configurée selon paramètres du registre application 
	 *
	 * @return Mailer
	 */
	function get()
	{
		if ( $this->cache )
			return $this->cache;
		
		
		// obtenir facade
		$f = $this->getFacade();		
		if ( $f )
		{
			try
			{
				// créer l'objet Mailer avec la stratégie MailSender obtenue
				$this->cache = new Mailer($f->getActive());
				return $this->cache;
			}
			catch( \Exception $e )
			{
				mail(\K_NETTOOLS_POSTMASTER, 'MailerFactoryFromRegistry::get()', $e->getMessage(), 'From: ' . 'exception@' . $_SERVER['HTTP_HOST']);
			}
		}

		
		return Mailer::getDefault();
	}
	
}


?>
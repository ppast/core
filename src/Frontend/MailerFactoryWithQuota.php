<?php

// namespace
namespace Ppast\Core\Frontend;



// clauses use
use Ppast\Core\MailSendersRegistry\QuotaFacade;
use Ppast\Core\MailSendersRegistry\Registry;
use Ppast\Core\MailSendersRegistry\ParamsManagerQuotaInterface;





// classe d'aide pour création d'un objet Mailer personnalisé depuis configuration registre applicatif, avec gestion quotas
class MailerFactoryWithQuota extends MailerFactoryFromRegistry
{
	protected $quotaKey;
	
	
	
	/**
	 * Constructeur
	 *
	 * @param ParamsManager $pm Objet d'accès aux paramètres
	 * @param string $key Clef de registre pour stratégie d'envoi active
	 * @param string $data Clef de regitre pour données de configuration pour les stratégies configurées
	 * @param string $quotaKey Clef de registre pour stockage des quotas d'envois
	 */
	public function __construct(ParamsManager $pm, $key = 'msender', $data = 'msenders_data', $quotaKey = 'msenders_quotas')
	{
		parent::__construct($pm, $key, $data);
		$this->quotaKey = $quotaKey;
	}
	
	
	
	/**
	 * Obtenir un objet facade approprié
	 *
	 * @param Registry $reg
	 * @return Ppast\Core\MailSendersRegistry\QuotaFacade
	 */
	protected function _getFacade(Registry $reg)
	{
		return new QuotaFacade(parent::_getFacade($reg), new ParamsManagerQuotaInterface($this->pm, $this->quotaKey));
	}
}


?>
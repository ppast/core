<?php

// namespace
namespace Ppast\Core\Frontend;






// classe d'aide pour accès registre application depuis la partie publique
class PublicInterface
{
	public $dbpm = NULL;
	public $mfactory = NULL;
	
	
	
	/**
	 * Constructeur interface publique
	 *
	 * @param ParamsManager $pm
	 * @param null|MailerFactory $mailer
	 */
	public function __construct(ParamsManager $pm, ?MailerFactory $mf = null)
	{
		$this->dbpm = $pm;
		$this->mfactory = $mf ? $mf : new MailerFactory();
	}
	

		
		
	/**
	 * Obtenir un chemin absolu	depuis racine serveur
	 *
	 * @param string $subdir
	 * @return string
	 */
	public static function absolutePathTo($subdir)
	{
		return $_SERVER['DOCUMENT_ROOT'] . $subdir;
	}
	
	
	
	/**
	 * Transformer un chemin absolu depuis racine serveur en relatif depuis racine web www
	 *
	 * @param string $dir
	 * @return string
	 */	 
	public static function relativePathTo($dir)
	{
		return str_replace($_SERVER['DOCUMENT_ROOT'], '', $dir);
	}
	

	
	/**
	 * Obtenir registre applicatif
	 * 
	 * @return ParamsManager
	 */
	public function getDbpm()
	{
		return $this->dbpm;
	}
	
	
	
	/**
	 * Obtenir factory mailer
	 * 
	 * @return MailerFactory
	 */
	public function getMailerFactory()
	{
		return $this->mfactory;
	}
	
	
	
	/**
	 * Obtenir une instance Mailer configurée pour la stratégie d'envoi active
	 *
	 * @return Mailer
	 */
	public function getMailer()
	{
		return $this->getMailerFactory()->get();
	}
	
}


?>